package actions

import "io/ioutil"

// File names
var (
	PublicCertificate = "public.crt"
	PrivateKey        = "private.key"
	EncryptFile       = "content-encrypt.txt"
	DecryptFile       = "content.txt"
)

// ReadFile returns the byte array with the file 'path' content
func ReadFile(path string) string {
	result, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	return string(result)
}

// WriteFile creates a new file
func WriteFile(fileName, content string) {
	err := ioutil.WriteFile(fileName, []byte(content), 0666)
	if err != nil {
		panic(err)
	}
}
