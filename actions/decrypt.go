package actions

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"path/filepath"
)

// Decrypt decrypts the message provided
func Decrypt() string {
	// 1. Read private key file
	privateKey := ReadFile(fmt.Sprintf(".%c%s", filepath.Separator, PrivateKey))

	// 2. Decrypt
	message := ReadFile(fmt.Sprintf(".%c%s", filepath.Separator, EncryptFile)) // Read file to decrypt
	result, err := decryptWitPrivateKey(privateKey, []byte(message))

	if err != nil {
		panic(err)
	}

	fmt.Println(result) // Show result in console
	return result
}

// GetPrivateKeyObject returns the private key object instance
func GetPrivateKeyObject(key string) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode([]byte(key))
	if block == nil {
		return nil, errors.New("not_pem_encoded")
	}

	if block.Type != "RSA PRIVATE KEY" {
		return nil, errors.New("not_rsa_private_key")
	}

	keyResult, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	return keyResult, err
}

// Decrypt a message with the private key provided
func decryptWitPrivateKey(key string, in []byte) (string, error) {
	keyResult, err := GetPrivateKeyObject(key)
	if err != nil {
		return "", err
	}

	out, err := rsa.DecryptPKCS1v15(rand.Reader, keyResult, in)
	if err != nil {
		return "", err
	}

	return string(out), err
}
