package actions

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"path/filepath"
)

// Encrypt encrypt the message provided
func Encrypt() string {
	// 1. Read public certificate file
	publicCRT := ReadFile(fmt.Sprintf(".%c%s", filepath.Separator, PublicCertificate))

	// 2. Read file with the text to encrypt
	message := ReadFile(fmt.Sprintf(".%c%s", filepath.Separator, DecryptFile))

	// 3. Encrypt the message
	result, err := encryptWithCertificate([]byte(publicCRT), message)
	if err != nil {
		panic(err)
	}

	// 4. Handle result
	resultString := string(result)
	WriteFile(EncryptFile, resultString) // Create the output file
	return resultString
}

// Encrypt a message with the certificate provided
func encryptWithCertificate(certificate []byte, message string) ([]byte, error) {
	block, _ := pem.Decode(certificate)
	if block == nil {
		return nil, errors.New("not_pem_encoded")
	}

	certificateObj, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, err
	}

	rsaPub, ok := certificateObj.PublicKey.(*rsa.PublicKey)
	if !ok {
		return nil, err
	}

	out, err := rsa.EncryptPKCS1v15(rand.Reader, rsaPub, []byte(message))
	if err != nil {
		return nil, err
	}

	return out, err
}
