package main

import (
	"fmt"
	"os"
	"text/tabwriter"

	"bitbucket.org/algorath/encrypt/actions"
)

// Actions
var (
	ActionDecrypt       = "decrypt"
	ActionEncrypt       = "encrypt"
	GenerateCertificate = "generate-crt"
)

func main() {
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) == 0 {
		showHelpMessage()
		return // Exit
	}

	// Execute command provided
	command := argsWithoutProg[0]
	switch command {
	case ActionEncrypt:
		actions.Encrypt()
	case ActionDecrypt:
		actions.Decrypt()
	case GenerateCertificate:
		actions.GenerateCRT()
	default:
		fmt.Printf("The action '%s' cannot be executed.\n", command)
		showHelpMessage()
	}
}

func showHelpMessage() {
	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 0, 8, 0, '\t', 0) // Format in tab-separated columns with a tab stop of 8.

	generateCRTCommand := fmt.Sprintf("  - %s:\t %s", GenerateCertificate, "Generate a public certificate")
	encryptCommand := fmt.Sprintf("  - %s:\t %s", ActionEncrypt, "Encrypt some text")
	decryptCommand := fmt.Sprintf("  - %s:\t %s", ActionDecrypt, "Decript text")

	fmt.Fprint(w, "Please use one of the following actions:\n\n")
	fmt.Fprintln(w, generateCRTCommand)
	fmt.Fprintln(w, encryptCommand)
	fmt.Fprintln(w, decryptCommand)
	w.Flush()
}
